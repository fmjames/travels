<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>Travels</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
              type="text/css" />
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>


    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('fonts/ionicons/css/ionicons.min.css')}}" media="all">

    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}" media="all">

    <link rel="stylesheet" href="{{asset('fonts/flaticon/font/flaticon.css')}}" media="all">

    <link rel="stylesheet" href="{{asset('fonts/fontawesome/css/font-awesome.min.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/select2.css')}}" media="all">


    <link rel="stylesheet" href="{{asset('css/helpers.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" media="all">



    </head>
	<body>


    <section class="probootstrap-cover overflow-hidden relative"   style="background-image: url('{{asset('images/cover_bg_1.jpg')}}'); background-attachment: fixed; min-height: 625px;" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        @yield('content')
      </div>

    </section>
    <!-- END section -->





    <script src="{{asset('js/jquery.min.js')}}"></script>

    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>

    <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>

    <script src="{{asset('js/select2.min.js')}}"></script>

    <script src="{{asset('js/main.js')}}"></script>



	</body>
</html>
