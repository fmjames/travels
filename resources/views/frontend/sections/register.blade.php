@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')
    <div class="container" style="background-color: #00ffff8c">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card" style="color: crimson;">
                    <div class="card-header text-center">
                        <h5>Create Your Account</h5>
                    </div>
                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('message'))
                            <div class="alert alert-{{session('type')}}">
                                {{session('message')}}
                            </div>
                            @endif

                        <form action="{{route('register')}}" method="post" class="form">
                            @csrf
                            <div class="form-group">
                                <label for="full_name">Full Name</label>
                                <input type="text" name="full_name" class="form-control" id="full_name" placeholder="Enter name" value="{{old('full_name')}}">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{old('email')}}">
                            </div>
                            <div class="form-group">
                                <label for="phone_number">Phone Number</label>
                                <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="Enter Phone Number" value="{{old('phone_number')}}">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="password">Re-Type Password</label>
                                <input type="password" name="password_confirmation" class="form-control" id="password" placeholder="Password" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                        Allready Have an Account!!! <a class="reg-link" href="{{route('login')}}">Login</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <style>
        .reg-link:hover{
            font-size: 150%;
            background-color: limegreen;
        }
    </style>
@endsection
