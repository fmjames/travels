@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')


        <div class="container" style="max-width: 1300px;">
            <div class="row">
                <div class="col-md-12">

                    <form method="post" action="{{route('show-tracks')}}" role="form" id="tracks_search" class="form-control">
                        @CSRF
                        <div class="">
                            <div class="form-group" style="display: flex; flex-flow: row">

                                <div class="input-field" style="margin-right: 10px; width: 300px;">
                                    <label for="start_loc">Leaving From:</label>
                                    <select class="js-example-basic-single js-states form-control" name="start_loc" id="start_loc">
                                        <option>Select Location</option>
                                        @foreach($locations as $location_data)
                                            <option value="{{$location_data->id}}" {{ $location_data->id == $start_loc ? "Selected": null}}>
                                                {{$location_data->loc}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="input-field" style="margin-right: 10px; width: 300px;">
                                    <label for="end_loc">Going To:</label>
                                    <select class="js-example-basic-single js-states form-control" name="end_loc" id="end_loc">
                                        @foreach($locations as $location_data)
                                            <option value="{{$location_data->id}}" {{ $location_data->id == $end_loc ? "Selected": null}}>
                                                {{$location_data->loc}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                    <div class="input-field" style="margin-right: 10px; width: 300px;">
                                        <label for="coach_type">Coach Type:</label>
                                        <select class="js-example-basic-single js-states form-control" name="coach_type" id="coach_type">
                                            <option value="AC" {{$coach_type=="AC" ? "Selected":null}}>AC</option>
                                            <option value="Non-AC" {{$coach_type=="Non-AC" ? "Selected":null}}>Non-AC</option>
                                        </select>
                                    </div>

                                    <div class="input-field" style="margin-right: 10px; width: 300px;">
                                        <label for="date">Journey Date:</label>
                                        <input name="date" type="text" autocomplete="off" class="form-control" id="date" value="{{$date}}" readonly style="background-color: white;">
                                    </div>

                                    <div class="input-field" style="margin-right: 10px; width: 300px;">
                                        <input type="submit" value="Submit" class="btn btn-primary" id="search" style="width: 250px; margin-top: 28px; margin-left: 10px; padding: 9px;">
                                    </div>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>


    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-lg-12">
                        <h2>Available Buses</h2>
                        <table class="table">
                            <thead class="table-dark" style="background-color: #00ffd0b8; color: white;">
                            <tr>
                                <th scope="col">SL NO</th>
                                <th scope="col">Buses Info</th>
                                <th scope="col">Routes Info</th>
                                <th scope="col">Dates</th>
                                <th scope="col">Fare</th>
                                <th scope="col">Departure Time</th>
                                <th scope="col">Arrival Time</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody class="table-light" style="background-color: #00ffd04a; color: white;">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(session()->has('message'))
                                <div class="alert alert-{{session('type')}}">
                                    <li>{{session('message')}}</li>

                                </div>
                            @endif
{{--                            {{dd($sorted_track)}}--}}
{{--                            @foreach($sorted_track as $single_track)--}}
{{--                                {{dd($single_track)}}--}}
                            @foreach($sorted_track as $key=>$tracks_info)
                                <tr>
{{--                                    {{dd($tracks_info)}}--}}
                                    <td>{{$key+1}}</td>
                                    <td>
                                        {{$tracks_info->buses_info->coach_id}} -
                                        {{$tracks_info->buses_info->coach_type}} -
                                        {{$tracks_info->buses_info->total_seat_no}}
                                    </td>

                                    <td>
                                        {{$tracks_info->routes_info->location_start->loc}} -
                                        {{$tracks_info->routes_info->location_end->loc}}
                                    </td>
                                    <td>
                                        {{$date}}

                                    </td>

                                    <td>{{$test_fare ?? ''}}</td>
                                    <td>{{$tracks_info->departure_time}}</td>
                                    <td>{{$tracks_info->arrival_time}}</td>
                                    <td>
                                        <a class="btn-sm btn-success" href="{{route('show-bus', [$tracks_info->id, $date])}}">View</a>
                                    </td>
                                </tr>
                            @endforeach
{{--                            @endforeach--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>








    <script type="text/javascript">
        // Get location info
        jQuery(document).ready(function ()
        {
            jQuery('select[name="start_loc"]').on('change',function(){
                var s_loc_ID = jQuery(this).val();
                if(s_loc_ID)
                {
                    jQuery.ajax({
                        url : 'search/' +s_loc_ID,
                        type : "GET",
                        dataType : "json",
                        // async : true,

                        success:function(dataSet)
                        {
                            var data = JSON.parse(JSON.stringify(dataSet));
                            // console.log(data);

                            jQuery('select[name="end_loc"]').empty();
                            jQuery.each(data, function(key,value){

                                console.log(value);
                                $('select[name="end_loc"]').append('<option value="'+ value.id +'">'+ value.loc +'</option>');
                            });
                        }
                    });
                }
                else
                {
                    $('select[name="route_id"]').empty();
                }
            });
        });
    </script>

    <script>
        // Get today's date
        var today = new Date();

        $("#date").datepicker({
            format: 'd-M-yyyy',
            changeMonth: true,
            changeYear: true,
            minDate: today // set the minDate to the today's date
            // you can add other options here
        });
    </script>


@endsection
