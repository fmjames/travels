@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')

    <div class="container" style="background-color: #00ffff8c">

        <div class="row justify-content-center">
            <div class="col-md-12 card text-center">
                <nav class="col-md-12 navbar navbar-light bg-light">
                    <a class="navbar-brand" href="#">Profile</a>
                </nav>
                <div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-{{session('type')}}">
                            <li>{{session('message')}}</li>
                        </div>
                    @endif
                </div>
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('profile')}}">INFORMATION</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{route('purchsae-history')}}">PURCHASE HISTORY</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless table-hover">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Booking Date:</th>
                                <th>From:</th>
                                <th>TO:</th>
                                <th>Journey Date:</th>
                                <th>Seat NO:</th>
                                <th>Total Fare:</th>
                                <th>Ticket ID:</th>
                                <th>Action:</th>
                            </tr>
                            </thead>

                            @foreach($ticket_info as $key=>$single_info)
                            <tbody>
                            <td>{{$key}}</td>
                            <td>{{$single_info->created_at->format('d M Y')}}</td>
                            <td>{{$single_info->tracks_info->routes_info->location_start->loc}}</td>
                            <td>{{$single_info->tracks_info->routes_info->location_end->loc}}</td>
                            <td>{{$single_info->journey_date}}</td>
{{--                            <td>{{$single_info->seat_id}}</td>--}}
                            <td>
                                @foreach(json_decode($single_info->seat_id) as $seats)
                                    <span class="badge badge-primary">{{ $seats ?? '' }}</span>
                                @endforeach
                            </td>
                            <td>{{$single_info->total_fare}}</td>
                            <td>{{$single_info->ticket_id}}</td>
                            <td>
                                <a class="btn-sm btn-danger" onclick="return confirm('Are you sure?')" href="{{route('delete-ticket',$single_info->id)}}">Delete</a>
                            </td>
                            </tbody>
                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
