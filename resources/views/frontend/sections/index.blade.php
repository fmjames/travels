@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')

    <div class="row align-items-center" style="padding: 0;">
        <div class="col-md">
            <h2 class="heading mb-2 display-4 font-light probootstrap-animate">Explore The World With Ease</h2>
            <p class="lead mb-5 probootstrap-animate"></p>
            <a href="#" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">See OnePage Version</a>
        </div>

        <div class="col-md probootstrap-animate" style="margin-left: 200px;">
            <form method="post" action="{{route('show-tracks')}}" role="form" id="route_search" class="probootstrap-form">
                <div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                @CSRF
                <div class="col-md">
                    <div class="form-group">
                        <div class="input-field">
                            <label for="start_loc">Leaving From:</label>
                            <select class="js-example-basic-single js-states form-control" name="start_loc" id="start_loc">
                                <option value="" selected>Select Location</option>
                                @foreach($locations as $location_data)
                                    <option value="{{$location_data->id}}">
                                        {{$location_data->loc}}
                                    </option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-field">
                        <label for="end_loc">Going To:</label>
                        <select class="js-example-basic-single js-states form-control" name="end_loc" id="end_loc">
                            <option value="">Select Location</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-field">
                        <label for="coach_type">Coach Type:</label>
                        <select class="js-example-basic-single js-states form-control" name="coach_type" id="coach_type">
                            <option value="AC">AC</option>
                            <option value="Non-AC">Non-AC</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-field">
                        <label for="date">Journey Date:</label>
                        <input readonly name="date" type="text" autocomplete="off" class="form-control" placeholder="dd/mm/yyyy" id="date" style="background-color: white">
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" value="Submit" class="btn btn-primary btn-block">
                </div>

                    </div>
            </form>

        </div>
    </div>

<script type="text/javascript">
        // Get location info
        jQuery(document).ready(function ()
        {
            jQuery('select[name="start_loc"]').on('change',function(){
                var s_loc_ID = jQuery(this).val();
                // console.log(s_loc_ID);
                if(s_loc_ID)
                {
                    jQuery.ajax({
                        url : 'search/' +s_loc_ID,
                        type : "GET",
                        dataType : "json",
                        // async : true,

                        success:function(dataSet)
                        {
                            console.log(dataSet);
                            var data = JSON.parse(JSON.stringify(dataSet));
                            // console.log(data);

                            jQuery('select[name="end_loc"]').empty();
                            jQuery.each(data, function(key,value){
                                // console.log(value);
                                $('select[name="end_loc"]').append('<option value="'+ value.id +'">'+ value.loc +'</option>');

                            });
                        }
                    });
                }
                else
                {
                    $('select[name="route_id"]').empty();
                }
            });
        });
    </script>

    <script>
        // Get today's date
        var today = new Date();

        $("#date").datepicker({
            format: 'd-M-yyyy',
            changeMonth: true,
            changeYear: true,
            minDate: today // set the minDate to the today's date
            // you can add other options here
        });
    </script>


@endsection
