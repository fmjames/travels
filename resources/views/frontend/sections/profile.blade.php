@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')

    <div class="container" style="background-color: #00ffff8c">

        <div class="row justify-content-center">
            <div class="col-md-12 card text-center">
                <nav class="col-md-12 navbar navbar-light bg-light">
                    <a class="navbar-brand" href="#">Profile</a>
                </nav>
                <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('profile')}}">INFORMATION</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('purchsae-history')}}">PURCHASE HISTORY</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <table class="table card table-borderless table-hover">
                        <tbody class="card-body">
                        <tr>
                            <th>Name:</th>
                            <td>{{auth()->user()->name}}</td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td>{{auth()->user()->email}}</td>
                        </tr>
                        <tr>
                            <th>Phone:</th>
                            <td>{{auth()->user()->phone_number}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
