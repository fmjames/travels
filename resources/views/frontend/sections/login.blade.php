@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card" style="color: crimson;">
                    <div class="card-header text-center">
                        <h5>Login Into Your Account</h5>
                    </div>
                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('message'))
                            <div class="alert alert-{{session('type')}}">
                                <li>{{session('message')}}</li>
                            </div>
                        @endif



                        <form action="{{route('login')}}" method="post" class="form">
                            @csrf
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{old('email')}}">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                            </div>
                        </form>

                            Not Registered Yet!!. <a class="reg-link" href="{{route('register')}}">Register</a> Now.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <style>
        .reg-link:hover{
            font-size: 150%;
            background-color: limegreen;
        }
    </style>
@endsection
