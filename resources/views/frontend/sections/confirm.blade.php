@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')
    <div class="container" style="background-color: #00ffff8c">
        <div class="row">
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-{{session('type')}}">
                        <li>{{session('message')}}</li>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
