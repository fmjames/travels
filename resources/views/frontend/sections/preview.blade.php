@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')
<div class="container" style="background-color: #00ffff8c">
<div class="row">

    <div class="col-md-3">
    </div>

    <div class="row col-md-6">
        <div class="card-header col-md-12" style="text-align: center; background-color: crimson; color: white;text-shadow: 2px 2px 5px white; font-size: 1.5em; padding: 0px;">
            Ticket Details
        </div>
        <table class="table form-group table-light" style="color: crimson">


            <tbody>
            <tr>
                <td>Name:</td>
                <td>{{auth()->user()->name ?? ''}}</td>
            </tr>
            <tr>
                <td>Phone NO:</td>
                <td>{{auth()->user()->phone_number ?? ''}}</td>
            </tr>
            <tr>
                <td>From:</td>
                <td>{{\Illuminate\Support\Facades\Session::get('booking')[0]->routes_info->location_start->loc}}</td>

            </tr>
            <tr>
                <td>To:</td>
                <td>{{\Illuminate\Support\Facades\Session::get('booking')[0]->routes_info->location_end->loc}}</td>

            </tr>
            <tr>
                <td>Date:</td>
                <td>{{\Illuminate\Support\Facades\Session::get('booking')[1]}}</td>

            </tr>
            <tr>
                <td>Departure Time:</td>
                <td>{{\Illuminate\Support\Facades\Session::get('booking')[0]->departure_time}}</td>

            </tr>
            <tr>
                <td>Selected Seat:</td>
                <td>
                    @foreach( \Illuminate\Support\Facades\Session::get('booking')[3] as $seat )
                        <span class="badge badge-danger">{{ $seat ?? '' }}</span>
                    @endforeach
                </td>
            </tr>
            <tr>
                <td>Total Fare:</td>
                <td>
                    <span class="badge badge-success">{{\Illuminate\Support\Facades\Session::get('booking')[2]}}</span>
                                        <span class="badge badge-success">{{ $fare ?? '' }}</span>
                </td>
            </tr>

                        <tr>
                <td>Ticket ID:</td>
                <td>{{\Illuminate\Support\Facades\Session::get('booking')[4]}}</td>

            </tr>
            </tbody>

            <tfoot>
            <tr>
                @auth()
                    <td>
                        <a href="{{route('confirm')}}" class="btn btn-sm btn-outline-primary" style="padding: 5px;">Proceed to Pay</a>
                    </td>
                @endauth
                @guest()
                    <td>
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" style="padding: 5px;">
                            Login to Proceed
                        </button>
                    </td>
                @endguest
            </tr>

            </tfoot>

        </table>
        </div>
    </div>

    <div class="col-md-3">
    </div>
</div>


{{--Modal Function--}}
<div class="modal fade" id="exampleModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Login to Your Account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="card" style="color: crimson;">
                    <div class="card-body">
                        <form action="{{route('relogin')}}" method="post" class="form">
                            @csrf
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{old('email')}}">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@endsection
