@extends('frontend.master')
@extends('frontend.partials.navbar')
@section('content')

<style>
    input#seat{
        padding: 10px;
        margin-bottom: 10px;
    }
    .single_seat{
        background-color: #00ca4c;
        color: white;
    }
    .check{
        background-color: #0062cc;
        color: white;
    }
    h5{
        color: white;
    }


</style>

<div class="container" style="background-color: #00ffff8c">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">

                <form class="form-control" style="width: auto">
                    <div class="btn-group btn-group-sm"  id="col1Buttons" style="display: inline-grid; padding: 10px;">
                    </div>
                    <div class="btn-group btn-group-sm"  id="col2Buttons" style="display: inline-grid; margin-right: 15px;">
                    </div>
                    <div class="btn-group btn-group-sm"  id="col3Buttons" style="display: inline-grid; margin-left: 15px;">
                    </div>
                    <div class="btn-group btn-group-sm"  id="col4Buttons" style="display: inline-grid; padding: 10px;">
                    </div>
                </form>

{{--                <form class="form-control" style="width: auto">--}}
{{--                    @for($j=65; $j<65+$seat_number; $j++)--}}

{{--                    <div class="btn-group btn-group-sm"  id="col1Buttons" style="display: inline-grid; padding: 10px;">--}}
{{--                        <input @if(in_array(chr($j).$test,$all_seats)) disabled style="background-color: red;" @endif class="btn single_seat" name="options"  type="button" id="seat" value="{{chr($j)}}1"/>--}}
{{--                    </div>--}}
{{--                    <div class="btn-group btn-group-sm"  id="col2Buttons" style="display: inline-grid; margin-right: 15px;">--}}
{{--                        <input class="btn single_seat" name="options"  type="button" id="seat" value="{{chr($j)}}2"/>--}}
{{--                    </div>--}}
{{--                    <div class="btn-group btn-group-sm"  id="col3Buttons" style="display: inline-grid; margin-left: 15px;">--}}
{{--                        <input class="btn single_seat" name="options"  type="button" id="seat" value="{{chr($j)}}3"/>--}}
{{--                    </div>--}}
{{--                    <div class="btn-group btn-group-sm"  id="col4Buttons" style="display: inline-grid; padding: 10px;">--}}
{{--                        <input class="btn single_seat" name="options"  type="button" id="seat" value="{{chr($j)}}4"/>--}}
{{--                    </div>--}}

{{--                    @endfor--}}
{{--                </form>--}}

            </div>
        </div>


        <div class="col-md-2">
            <h5 style="color: white">Seat Details</h5>
            <div class="btn-group btn-group-sm" data-toggle="buttons" style="padding-bottom: 10px;">
                <input class="btn btn-primary"  type="button" value="Available" style="pointer-events: none">
            </div>

            <div class="btn-group btn-group-sm" data-toggle="buttons" style="padding-bottom: 10px;">
                <input class="btn btn-primary"  type="button" value="Selected" style="background-color: #0062cc; pointer-events: none; padding-right: 12px;">
            </div>

            <div class="btn-group btn-group-sm" data-toggle="buttons">
                <input class="btn btn-primary"  type="button" value="Sold"  style=" pointer-events: none; margin-right: 6px; padding-right: 54px; background-color: red">
            </div>

        </div>


        <div class="row col-md-6">
            <table class="table form-group">
                <tr class="col-md-6">
                    <td>
                        <div class="card">
                            <div class="card-header" style="background-color: crimson; padding: 0px; margin: 0px;">
                                <h5 align="center">Destination Details</h5>
                            </div>
                            <div class="card-body" style="padding: 0rem;">
                                <table class="table">
                                    <thead class="">
                                    <tr>
                                        <th scope="col">From-To</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Departing Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="col">{{$tracksdata->routes_info->location_start->loc}} -
                                            {{$tracksdata->routes_info->location_end->loc}}</th>
                                        <th scope="col">{{$date}}</th>
                                        <th scope="col">{{$tracksdata->departure_time}}</th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="col-md-6">
                    <td>
                        <div>
                            <div class="card">
                                <div class="card-header" style="background-color: crimson; padding: 0px; margin: 0px;">
                                    <h5 align="center">Seat Information</h5>
                                </div>
                                <div class="card-body">
                                    <table class="table">
                                        <tbody class="">
                                        <tr>
                                            <td scope="col">Seat NO.</td>
                                            <td scope="col" align="center">Fare</td>
                                            <td scope="col" align="right">Remove</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                              <td scope="col">
                                                  <div>
                                                      @if ($errors->any())
                                                          <div class="alert alert-danger">
                                                              <ul>
                                                                  @foreach ($errors->all() as $error)
                                                                      <li>{{ $error }}</li>
                                                                  @endforeach
                                                              </ul>
                                                          </div>
                                                      @endif
                                                      @if(session()->has('message'))
                                                          <div class="alert alert-{{session('type')}}">
                                                              <li>{{session('message')}}</li>
                                                          </div>
                                                      @endif
                                                  </div>



                                                <form method="post" action="{{route('booking')}}" role="form" id="seat_fare" class="form-group">
                                                    @csrf
                                                    <input type="hidden" name="tracks_info" value="{{$tracksdata->id}}">
                                                    <input type="hidden" name="date" value="{{$date}}">
                                                </form>
                                                <div class="form-group">
                                                    <p id="total_fare" style="color: crimson"></p>
                                                    <input type="submit" value="Submit" class="btn btn-primary" form="seat_fare">
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>


    </div>
</div>

@for($j=65; $j<65+$seat_number; $j++)
<script>
    var pages = {{$seat_number}};
    // console.log(pages);
    var page1Buttons = $('#col1Buttons');
    var page2Buttons = $('#col2Buttons');
    var page3Buttons = $('#col3Buttons');
    var page4Buttons = $('#col4Buttons');

    // for (var j=65; j<65+pages; j++){

        page1Buttons.append('<input @if(in_array(chr($j).$col1,$all_seats)) style="background-color: red; pointer-events: none;" @endif class="btn single_seat" name="options"  type="button" id="seat" value="' + String.fromCharCode({{$j}}) + '' + 1 + '"/>');
        page2Buttons.append('<input @if(in_array(chr($j).$col2,$all_seats)) style="background-color: red; pointer-events: none;" @endif class="btn single_seat" name="options"  type="button" id="seat" value="' + String.fromCharCode({{$j}}) + '' + 2 + '"/>');
        page3Buttons.append('<input @if(in_array(chr($j).$col3,$all_seats)) style="background-color: red; pointer-events: none;" @endif class="btn single_seat" name="options"  type="button" id="seat" value="' + String.fromCharCode({{$j}}) + '' + 3 + '"/>');
        page4Buttons.append('<input @if(in_array(chr($j).$col4,$all_seats)) style="background-color: red; pointer-events: none;" @endif class="btn single_seat" name="options"  type="button" id="seat" value="' + String.fromCharCode({{$j}}) + '' + 4 + '"/>');
    // }
</script>
@endfor



<script type="text/javascript">

        maxticket=1;
        $(document).ready(function () {

            $(".single_seat").on('click', function () {
                var seat_ID = '';
                var seat_ID = $(this).val();
                // console.log(seat_ID);
                // console.log(maxticket);
                if ($(this).hasClass('single_seat') && maxticket<=3) {
                    maxticket++;
                    // console.log(maxticket);
                    $(this).removeClass('single_seat');
                    $(this).addClass('check');
                    var trData = `<tr id=` + seat_ID + ` class="remove">
                                        <td>
                                        <input type="text" class="form-control" id="selected_seat" name="selected_seat[]" value="` + seat_ID + `" readonly required style="border:none; background-color:#ffffff">
                                        </td>
                                        <td>
                                        <input type="number" class="form-control seat_fare" id="seat_fare" name="seat_fare[]" value="{{$tracksdata->fare}}" readonly required style="border:none; background-color:#ffffff">
                                        </td>
                                    <td><button class="remove_row btn btn-sm btn-danger" id=` + seat_ID + ` type="button">-</button></td>
                                </tr>`;

                    $('form[id="seat_fare"]').append(trData);
                    // alert(seat_ID);

                }
                else if ($(this).hasClass('single_seat') && maxticket>3){
                    $(this).removeClass('single_seat');
                    $(this).addClass('single_seat');
                    alert("Maximum 3 tickets are allowed.");
                }
                else {

                    $(this).removeClass('check');
                    $(this).addClass('single_seat');
                    var r_id = '';
                    var r_id = $(this).val();
                    var r_id = "#".concat($(this).val());
                    $(r_id).closest('tr').remove();
                    // alert(r_id);
                    maxticket--;
                    // console.log(maxticket);
                }
            });


        });


    $(document).ready(function () {

        $( "#seat_fare" ).delegate('.remove_row', 'click', function(){

            var check_id = this.id;
            $('input[type=button]').each(function(){
                var text_value=$(this).val();
                if(text_value.indexOf(check_id) != -1)
                {
                    $(this).removeClass('check');
                    $(this).removeClass('active');
                    $(this).addClass('single_seat');
                }

            });

            $(this).closest('tr').remove();
            maxticket--;
        });

        // $(document).ready(function() {
        //         var $result = $('#total_fare');
        //         var $qtyInput = $('#seat_fare');
        //         $qtyInput.change(function() {
        //             var sum = 0;
        //
        //             $qtyInput.each(function() {
        //                 var value = parseFloat(this.value) || 0;
        //                 var price = parseFloat(this.dataset.price) || 0;
        //                 sum += value * price;
        //             });
        //
        //             $result.html(sum.toFixed(2))
        //         })
        //     });


    });

</script>

@endsection
