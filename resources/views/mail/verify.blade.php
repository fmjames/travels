<!doctype html>
<html lang="en">
<body>
<br>
    <p>Hello {{$user->name}}</p>
    <p>Your account has been created successfully. Please verify your account through following link</p>
    <a href="{{route('verify',$user->email_verification_token)}}">{{route('verify',$user->email_verification_token)}}</a>
    </br>
    <p>Thank You</p>
</div>
</body>
</html>
