@extends('backend.master')
@section('content')


    @if( $errors->any())
        <div class="alert alert-danger col-md-6">
            <ul>
                @foreach($errors->all() as $error)
                    <p>
                        {{$error}}
                    </p>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success col-md-6">
            {{ session()->get('message') }}
        </div>
    @endif


    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add New Buses
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #50de71">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Buses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #75ec9f">

                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="card card-default">
                                    <div class="card-header">
                                        Enter Buses Info
                                    </div>
                                    <div class="card-body">
                                        <form class="form-group" method="post" action="{{route('buses-insert')}}"
                                              role="form" id="s_location">
                                            @csrf
                                            <div class="form-group">
                                                <label for="b_id">Coach Id</label>
                                                <input type="number" min="1" class="form-control" name="coach_id"
                                                       id="b_id" placeholder="Coach Id">
                                            </div>

{{--                                            <div class="form-group">--}}
{{--                                                <label for="">Select Route</label>--}}
{{--                                                <select name="route_id" class="form-control">--}}
{{--                                                    @foreach($routes_data as $single_data)--}}
{{--                                                        <option--}}
{{--                                                            value="{{$single_data->id}}">{{$single_data->location_start->loc}}--}}
{{--                                                            -{{$single_data->location_end->loc}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
{{--                                            </div>--}}

                                            <div class="form-group">
                                                <label for="b_type">Bus Type</label>
                                                <select class="form-control" name="bus_type"
                                                        id="b_type">
                                                    <option value="AC">AC</option>
                                                    <option value="Non-AC">Non AC</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="b_seat_no">Total Seats On Bus</label>
                                                <input type="number" min="1" class="form-control" name="b_seat_no"
                                                       id="b_seat_no" placeholder="Bus Seat Number">
                                            </div>

                                            <div class="form-group">
                                                <button class="btn-sm btn-warning" type="reset" value="Reset">Reset
                                                </button>

                                                <button class="btn-sm btn-primary" type="submit" value="Submit"
                                                        form="s_location">Submit
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: #b0f5c0">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

{{--    --Modal End----}}



    <div class="container">
        <div class="col-md-12">
            <h2 class="">Buses Details</h2>
            <table class="table">
                <thead class="table-dark">
                <tr>
                    <th scope="col">SL NO</th>
                    <th scope="col">Coach-ID</th>
                    <th scope="col">Bus Type</th>
                    <th scope="col">Total Number of Seats</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody class="table-light">
                                @foreach($buses_data as $key=>$buses_info)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$buses_info->coach_id}}</td>
                                        <td>{{$buses_info->coach_type}}</td>
                                        <td>{{$buses_info->total_seat_no}}</td>
                                        <td>
                                            <a class="btn-sm btn-warning" href="{{route('buses-edit',$buses_info->id)}}">Edit</a>
                                            <a class="btn-sm btn-danger" href="{{route('buses-delete',$buses_info->id)}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection
