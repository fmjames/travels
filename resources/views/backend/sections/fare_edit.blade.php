@extends('backend.master')
@section('content')

    @if( $errors->any())
        <div class="alert alert-danger col-md-6">
            <ul>
                @foreach($errors->all() as $error)
                    <p>
                        {{$error}}
                    </p>
                @endforeach
            </ul>
        </div>
    @endif



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">Update Fare Details</h2>
                    </div>
                    <div class="card-body">
                        <form class="form-group" method="post" action="{{route('fare-update', $fare_id->id)}}" role="form" id="s_location">
                            @csrf
                            <div  class="form-group">
                                <label for="s_loc">Location From</label>
                                <select name="start_location" class="form-control" id="s_loc">
                                    @foreach($locations as $single_location)
                                        <option value="{{$single_location->id}}" {{$single_location->id == $fare_id->from ? "Selected": null}}>
                                            {{$single_location->loc}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div  class="form-group">
                                <label for="e_loc">Location To</label>
                                <select name="end_location" class="form-control" id="e_loc">
                                    @foreach($locations as $single_location)
                                        <option value="{{$single_location->id}}" {{$single_location->id == $fare_id->to ? "Selected": null}}>
                                            {{$single_location->loc}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="from-group">
                                <label for="c_type">Coach Type</label>
                                <select class="form-control" name="coach_type" id="c_type">
                                    <option value="AC">AC</option>
                                    <option value="Non-AC">Non-AC</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="fare">Fare</label>
                                <input type="number" min="1" class="form-control" name="fare"
                                       id="fare" value="{{$fare_id->fare}}">
                            </div>

                        </form>

                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <button class="btn-sm btn-warning" type="reset" value="Reset" form="s_location">Reset</button>

                            <button class="btn-sm btn-success" type="submit" value="Submit" form="s_location">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
