@extends('backend.master')
@section('content')




@if( $errors->any())
<div class="alert alert-danger col-md-6">
    <ul>
        @foreach($errors->all() as $error)
        <p>
            {{$error}}
        </p>
            @endforeach
    </ul>
</div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success col-md-6">
        {{ session()->get('message') }}
    </div>
@endif


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Add New Location
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #50de71">
                <h5 class="modal-title" id="exampleModalLabel" align="center">Add New Location</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color: #75ec9f">

                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <div class="card card-default">
                                <div class="card-header">
                                    Enter Location Info
                                </div>
                                <div class="card-body">
                                    <form class="form-group" method="post" action="{{route('loc-insert')}}" role="form" id="s_location">
                                        @csrf
                                        <div  class="form-group">
                                            <label for="s_loc">Location Name</label>
                                            <input type="text" name="location_name" placeholder="Location Name" id="s_loc" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <button class="btn-sm btn-warning" type="reset" value="Reset">Reset</button>

                                            <button class="btn-sm btn-primary" type="submit" value="Submit" form="s_location">Submit</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer" style="background-color: #b0f5c0">




                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>


            </div>
        </div>
    </div>
</div>

{{----Modal End----}}



<div class="container">
    <div class="col-md-12">
        <h2 class="title-1 m-b-25">Location Details</h2>
        <table class="table">
            <thead class="table-dark">
            <tr>
                <th scope="col">SL NO</th>
                <th scope="col">Locations</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody class="table-light">
            @foreach($loc_data as $key=>$loc_info)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$loc_info->loc}}</td>
                <td>
                    <a class="btn-sm btn-warning" href="{{route('loc-edit', $loc_info->id)}}">Edit</a>
                    <a class="btn-sm btn-danger" href="{{route('loc-delete', $loc_info->id)}}">Delete</a>
                </td>
            </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


    <a href="{{route('test')}}">Test</a>

@endsection
