@extends('backend.master')
@section('content')


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">Update Routes Info</h2>
                    </div>
    <div class="card-body">
        <form class="form-group" method="post" action="{{route('routes-update', $routes_data->id)}}" role="form" id="s_location">
            @csrf
            <div class="form-group">
                <label for="">Select Start Location</label>
                <select name="start_location" class="form-control">
                    <option value="">Select an Location</option>
                    @foreach($loc_data as $single_data)
                    <option value="{{$single_data->id}}" {{ $single_data->id == $routes_data->start_loc ? "Selected": null}}>
                        {{$single_data->loc}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="">Select End Location</label>
                <select name="end_location" class="form-control">
                    <option value="">Select an Location</option>
                    @foreach($loc_data as $single_data)
                        <option value="{{$single_data->id}}"  {{ $single_data->id == $routes_data->end_loc ? "Selected": null}}>
                            {{$single_data->loc}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="">Select Starting Points</label>
                <select name="start_points[]" class="form-control multiple-select" multiple>
                    @foreach($loc_data as $single_data)
                        <option value="{{$single_data->id}}">
                            {{$single_data->loc}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="">Select Ending Points</label>
                <select name="end_points[]" class="form-control multiple-select" multiple>
                    @foreach($loc_data as $single_data)
                        <option value="{{$single_data->id}}">
                            {{$single_data->loc}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button class="btn-sm btn-warning" type="reset" value="Reset">Reset
                </button>

                <button class="btn-sm btn-primary" type="submit" value="Submit"
                        form="s_location">Submit
                </button>
            </div>
        </form>

    </div>
    </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $(".multiple-select").bsMultiSelect();
        });

    </script>

@endsection
