@extends('backend.master')
@section('content')




    @if( $errors->any())
        <div class="alert alert-danger col-md-6">
            <ul>
                @foreach($errors->all() as $error)
                    <p>
                        {{$error}}
                    </p>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success col-md-6">
            {{ session()->get('message') }}
        </div>
    @endif


    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add Fares For Locations
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #50de71">
                    <h5 class="modal-title" id="exampleModalLabel" align="center">Add New Fare</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #75ec9f">

                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="card card-default">
                                    <div class="card-header">
                                        Enter Fare Info
                                    </div>
                                    <div class="card-body">
                                        <form class="form-group" method="post" action="{{route('fare-insert')}}" role="form" id="s_location">
                                            @csrf
                                            <div  class="form-group">
                                                <label for="s_loc">Location From</label>
                                                <select name="start_location" class="form-control" id="s_loc">
                                                    @foreach($location as $single_location)
                                                        <option value="{{$single_location->id}}">
                                                            {{$single_location->loc}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div  class="form-group">
                                                <label for="e_loc">Location To</label>
                                                <select name="end_location" class="form-control" id="e_loc">
                                                    @foreach($location as $single_location)
                                                        <option value="{{$single_location->id}}">
                                                            {{$single_location->loc}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="from-group">
                                                <label for="c_type">Coach Type</label>
                                                <select class="form-control" name="coach_type" id="c_type">
                                                    <option value="AC">AC</option>
                                                    <option value="Non-AC">Non-AC</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="fare">Fare</label>
                                                <input type="number" min="1" class="form-control" name="fare"
                                                       id="fare" placeholder="Enter Fare">
                                            </div>
                                            <div class="form-group">
                                                <button class="btn-sm btn-warning" type="reset" value="Reset">Reset</button>

                                                <button class="btn-sm btn-primary" type="submit" value="Submit" form="s_location">Submit</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer" style="background-color: #b0f5c0">




                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>


                </div>
            </div>
        </div>
    </div>

    {{----Modal End----}}



    <div class="container">
        <div class="col-md-12">
            <h2 class="title-1 m-b-25">Fare Details</h2>
            <table class="table">
                <thead class="table-dark">
                <tr>
                    <th scope="col">SL NO</th>
                    <th scope="col">From</th>
                    <th scope="col">TO</th>
                    <th scope="col">Coach Type</th>
                    <th scope="col">Fare</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody class="table-light">
                @foreach($fares as $key=>$fare)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$fare->location_from->loc}}</td>
                        <td>{{$fare->location_to->loc}}</td>
                        <td>{{$fare->coach_type}}</td>
                        <td>{{$fare->fare}}</td>
                        <td>
                            <a class="btn-sm btn-warning" href="{{route('fare-edit', $fare->id)}}">Edit</a>
                            <a class="btn-sm btn-danger" href="{{route('fare-delete', $fare->id)}}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
