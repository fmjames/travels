@extends('backend.master')
@section('content')

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add New Routes
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #50de71">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Routes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="background-color: #75ec9f">

                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="card card-default">
                                    <div class="card-header">
                                        Enter Routes Info
                                    </div>
                                    <div class="card-body">
                                        <form class="form-group" method="post" action="{{route('routes_add')}}"
                                              role="form" id="s_location">
                                            @csrf
                                            <div class="form-group">
                                                <label for="">Select Start Location</label>
                                                <select name="start_location" class="form-control">
                                                    @foreach($loc_data as $single_data)
                                                        <option
                                                            value="{{$single_data->id}}">{{$single_data->loc}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Select End Location</label>
                                                <select name="end_location" class="form-control">
                                                    @foreach($loc_data as $single_data)
                                                        <option
                                                            value="{{$single_data->id}}">{{$single_data->loc}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Select Starting Points</label>
                                                <select name="start_points[]" class="form-control multiple-select" multiple>
                                                    @foreach($loc_data as $single_data)
                                                        <option
                                                            value="{{$single_data->id}}">{{$single_data->loc}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Select Ending Points</label>
                                                <select name="end_points[]" class="form-control multiple-select" multiple>
                                                    @foreach($loc_data as $single_data)
                                                        <option
                                                            value="{{$single_data->id}}">{{$single_data->loc}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn-sm btn-warning" type="reset" value="Reset">Reset
                                                </button>

                                                <button class="btn-sm btn-primary" type="submit" value="Submit"
                                                        form="s_location">Submit
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: #b0f5c0">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{----Modal End----}}



    <div class="container">
        <div class="col-md-12">
            <h2 class="">Route Details</h2>
            <table class="table">
                <thead class="table-dark">
                <tr>
                    <th scope="col">SL NO</th>
                    <th scope="col">Start Point</th>
                    <th scope="col">End Point</th>
                    <th scope="col">Starting Locations</th>
                    <th scope="col">Ending Locations</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody class="table-light">
                @foreach($route_data as $key=>$routes_info)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$routes_info->location_start->loc}}</td>
                        <td>{{$routes_info->location_end->loc}}</td>
                        <td>

                            @foreach(json_decode($routes_info->starting_points) as $single_loc)
                                @foreach($loc_data as $data)
                                   @if($data->id==$single_loc)
                                       <span class="badge badge-info"> {{$data->loc}}</span>
                                       @endif
                                @endforeach
{{--                               {{$single_loc->location_start->loc}}--}}
                            @endforeach
                        </td>
                        <td>
                            @foreach(json_decode($routes_info->ending_points) as $single_loc)
                                @foreach($loc_data as $data)
                                    @if($data->id==$single_loc)
                                        <span class="badge badge-info"> {{$data->loc}}</span>
                                    @endif
                                @endforeach
                            @endforeach
                        </td>
                        <td>{{$routes_info->status}}</td>
                        <td>
                            <a class="btn-sm btn-warning" href="{{route('routes-edit', $routes_info->id)}}">Edit</a>
                            <a class="btn-sm btn-danger" href="{{route('routes-delete', $routes_info->id)}}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(function(){
            $(".multiple-select").bsMultiSelect();
        });

    </script>

@endsection
