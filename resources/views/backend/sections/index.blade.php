@extends('backend.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="overview-wrap">
                <h2 class="title-1">overview</h2>



                <div class="dropleft">
                    <button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown">
                        Add Info
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                        <li class="dropdown-item">
                            <a href="{{route('locations')}}">Locations</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="{{route('routes')}}">Routes</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="{{route('buses')}}">Buses</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="{{route('tracks')}}">Tracks</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="#">Transactions</a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9">
            <h2 class="title-1 m-b-25">Earnings By Items</h2>
            <table class="table table-borderless table-striped table-earning">
                <thead>
                <tr>
                    <th>date</th>
                    <th>order ID</th>
                    <th>name</th>
                    <th>price</th>
                    <th>quantity</th>
                    <th>total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>2018-09-29 05:57</td>
                    <td>100398</td>
                    <td>iPhone X 64Gb Grey</td>
                    <td class="text-right">$999.00</td>
                    <td class="text-right">1</td>
                    <td class="text-right">$999.00</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
