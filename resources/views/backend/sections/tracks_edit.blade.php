@extends('backend.master')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">Update Tracks Info</h2>
                    </div>
                    <div class="card-body">
                        <form class="form-group" method="post" action="{{route('tracks_update', $tracks_data->id)}}"
                              role="form" id="s_location">
                            @csrf
                            <div class="form-group">
                                <label for="">Select Buses Info</label>
                                <select name="buses_id" class="form-control">
                                    @foreach($buses_data as $single_data)
                                        <option
                                            value="{{$single_data->id}}" {{ $single_data->id == $tracks_data->buses_id ? "Selected": null}}>
                                            {{$single_data->coach_id}} --
                                            {{$single_data->type}} --
                                            {{$single_data->total_seat_no}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Select Routes Info</label>
                                <select name="routes_id" class="form-control">
                                    @foreach($routes_data as $single_data)
                                        <option
                                            value="{{$single_data->id}}" {{ $single_data->id == $tracks_data->routes_id ? "Selected": null}}>
                                            {{$single_data->location_start->loc}} --
                                            {{$single_data->location_end->loc}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="Txt_Date">Dates</label>
                                <input type="text" name="date" id="Txt_Date"
                                       value="{{$tracks_data->dates}}" class="form-control"
                                       style="cursor: pointer;">
                            </div>

                            <div class="form-group">
                                <label for="d_time">Departure Time</label>
                                <input type="time" name="dept_time" id="d_time" class="form-control"
                                       value="{{$tracks_data->departure_time}}">
                            </div>

                            <div class="form-group">
                                <label for="a_time">Arrival Time</label>
                                <input type="time" name="arrv_time" id="a_time" class="form-control"
                                       value="{{$tracks_data->arrival_time}}">
                            </div>

                            <div class="form-group">
                                <button class="btn-sm btn-warning" type="reset" value="Reset">Reset
                                </button>

                                <button class="btn-sm btn-primary" type="submit" value="Submit"
                                        form="s_location">Submit
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $("#Txt_Date").datepicker({
            format: 'd-M-yyyy',
            inline: false,
            lang: 'en',
            hour12: true,
            step: 30,
            multidate: 30,
            closeOnDateSelect: true
        });
    </script>




@endsection
