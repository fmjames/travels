@extends('backend.master')
@section('content')


    @if( $errors->any())
        <div class="alert alert-danger col-md-6">
            <ul>
                @foreach($errors->all() as $error)
                    <p>
                        {{$error}}
                    </p>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success col-md-6">
            {{ session()->get('message') }}
        </div>
    @endif


    <!-- Button trigger modal -->

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
         Add Tracks Info
    </button>


            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #50de71">
                            <h5 class="modal-title" id="exampleModalLabel"> Add Tracks Info</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="background-color: #75ec9f">

                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-md-8">
                                        <div class="card card-default">
                                            <div class="card-header">
                                                Enter Tracks Info
                                            </div>
                                            <div class="card-body">
                                                <form class="form-group" method="post" action="{{route('tracks-add')}}"
                                                      role="form" id="s_location">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="">Select Buses Info</label>
                                                        <select name="buses_id" class="form-control">
                                                            <option value="" selected>Select Bus</option>
                                                            @foreach($buses_data as $single_data)
                                                                <option
                                                                    value="{{$single_data->id}}">
                                                                    {{$single_data->coach_id}} --
                                                                    {{$single_data->coach_type}} --
                                                                    {{$single_data->total_seat_no}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="">Select Routes Info</label>
                                                        <select name="routes_id" class="form-control">
                                                            <option value="" selected>Select Route</option>
                                                            @foreach($route_data as $single_data)
                                                                <option
                                                                    value="{{$single_data->id}}">
                                                                    {{$single_data->location_start->loc}} --
                                                                    {{$single_data->location_end->loc}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>



                                                    <div  class="form-group">
                                                        <label for="Txt_Date">Select Dates</label>
                                                        <input type="text" name="date" id="Txt_Date" placeholder="Choose Date" class="form-control" autocomplete="off" style="cursor: pointer;">
                                                    </div>

                                                    <div  class="form-group">
                                                        <label for="d_time">Departure Time</label>
                                                        <input type="time" name="dept_time"id="d_time" class="form-control">
                                                    </div>

                                                    <div  class="form-group">
                                                        <label for="a_time">Arrival Time</label>
                                                        <input type="time" name="arrv_time" id="a_time" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                        <button class="btn-sm btn-warning" type="reset" value="Reset">Reset
                                                        </button>

                                                        <button class="btn-sm btn-primary" type="submit" value="Submit"
                                                                form="s_location">Submit
                                                        </button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" style="background-color: #b0f5c0">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            {{----Modal End----}}



    <div class="">
        <div class="col-md-12">
            <h2 class="">Tracks Details</h2>
            <table class="table col-md-8">
                <thead class="table-dark">
                <tr>
                    <th scope="col">SL NO</th>
                    <th scope="col">Buses Info</th>
                    <th scope="col">Routes Info</th>
                    <th scope="col">Dates</th>
                    <th scope="col">Departure Time</th>
                    <th scope="col">Arrival Time</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody class="table-light">
                @foreach($tracks_data as $key=>$tracks_info)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>
                            {{$tracks_info->buses_info->coach_id}} -
                            {{$tracks_info->buses_info->coach_type}} -
                            {{$tracks_info->buses_info->total_seat_no}}
                        </td>

                        <td>
                            {{$tracks_info->routes_info->location_start->loc}} -
                            {{$tracks_info->routes_info->location_end->loc}}
                        </td>
                        <td>

{{--                            <textarea readonly>--}}
{{--                                @foreach(json_decode($tracks_info->dates) as $dates)--}}
{{--                                    {{$dates}}--}}
{{--                                @endforeach--}}
{{--                                {{json_decode($tracks_info->dates)}}--}}
{{--                                @json($tracks_info->dates)--}}
{{--                            </textarea>--}}
                            <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="
                                @foreach(json_decode($tracks_info->dates) as $dates)
                                                                {{$dates}}
                                                            @endforeach">
                                Show Dates
                            </button>

                        </td>

                        <td>{{$tracks_info->departure_time}}</td>
                        <td>{{$tracks_info->arrival_time}}</td>
                        <td>
                            <a class="btn-sm btn-warning" href="{{route('tracks_edit', $tracks_info->id)}}">Edit</a>
                            <a class="btn-sm btn-danger" href="{{route('tracks_delete', $tracks_info->id)}}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


<script>
        $("#Txt_Date").datepicker({
            format: 'd-M-yyyy',
            inline: false,
            lang: 'en',
            step: 30,
            multidate: 30,
            closeOnDateSelect: true
        });
    </script>

<script>

        jQuery(document).ready(function ()
        {
            jQuery('select[name="buses_id"]').on('change',function() {
                var bus_ID = jQuery(this).val();
                console.log('dfsdhff');
                jQuery('input[name="date"]').on('select',function() {
                    var dates = jQuery(this).val();
                    console.log('dfsdhff');
                });
            });
        });
</script>





@endsection
