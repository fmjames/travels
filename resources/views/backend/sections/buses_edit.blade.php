@extends('backend.master')
@section('content')


    @if( $errors->any())
        <div class="alert alert-danger col-md-6">
            <ul>
                @foreach($errors->all() as $error)
                    <p>
                        {{$error}}
                    </p>
                @endforeach
            </ul>
        </div>
    @endif



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">Update Buses Info</h2>
                    </div>
                    <div class="card-body">
                        <form class="form-group" method="post" action="{{route('buses-update', $buses_info->id)}}"
                              role="form" id="s_location">
                            @csrf
                            <div class="form-group">
                                <label for="b_id">Coach Id</label>
                                <input type="number" min="1" class="form-control" name="coach_id" value="{{$buses_info->coach_id}}"
                                       id="b_id" placeholder="Coach Id">
                            </div>

                            <div class="form-group">
                                <label for="b_type">Bus Type</label>
                                <select class="form-control" name="bus_type"
                                        id="b_type">
                                    <option value="AC">AC</option>
                                    <option value="Non-AC">Non AC</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="b_seat_no">Total Seats On Bus</label>
                                <input type="number" min="1" class="form-control" name="b_seat_no" value="{{$buses_info->total_seat_no}}"
                                       id="b_seat_no" placeholder="Bus Seat Number">
                            </div>

                            <div class="form-group">
                                <button class="btn-sm btn-warning" type="reset" value="Reset">Reset
                                </button>

                                <button class="btn-sm btn-primary" type="submit" value="Submit"
                                        form="s_location">Submit
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
