@extends('backend.master')
@section('content')

    @if( $errors->any())
        <div class="alert alert-danger col-md-6">
            <ul>
                @foreach($errors->all() as $error)
                    <p>
                        {{$error}}
                    </p>
                @endforeach
            </ul>
        </div>
    @endif



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">
                        <h2 align="center">Update Location Name</h2>
                    </div>
                    <div class="card-body">
                        <form class="form-group" method="post" action="{{route('loc-update', $loc_id->id)}}" role="form" id="s_location">
                            @csrf
                            <div  class="form-group">
                                <label for="s_loc">Start Location Name</label>
                                <input type="text" name="location_name" placeholder="Start Location" id="s_loc" class="form-control" value="{{$loc_id->loc}}">
                            </div>

                        </form>

                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <button class="btn-sm btn-warning" type="reset" value="Reset" form="s_location">Reset</button>

                            <button class="btn-sm btn-success" type="submit" value="Submit" form="s_location">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
