<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard</title>


    <script type="text/javascript"
            src="https://www.viralpatel.net/demo/jquery/jquery.shorten.1.0.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>


    <!-- Fontfaces CSS-->
{{--    <link href="{{asset('backend/font-face.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/font-awesome.min.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/fontawesome-all.min.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">--}}
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{asset('fa/css/font-awesome.min.css')}}">
    <script src="https://kit.fontawesome.com/ca8d9b5d6a.js" crossorigin="anonymous"></script>



    {{--    <!-- Vendor CSS-->--}}
    <link href="{{asset('backend/animsition.min.css')}}" rel="stylesheet" media="all">
{{--    <link href="{{asset('backend/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/animate.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/hamburgers.min.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/slick.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/select2.min.css')}}" rel="stylesheet" media="all">--}}
{{--    <link href="{{asset('backend/perfect-scrollbar.css')}}" rel="stylesheet" media="all">--}}

    {{--    <!-- Main CSS-->--}}
    <link href="{{asset('backend/theme.css')}}" rel="stylesheet" media="all">
    {{--    <!-- Bootstrap CSS-->--}}
    <link href="{{asset('backend/bootstrap.min.css')}}" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" media="all">

{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">--}}

{{--    --Button CSS----}}
{{--    <link href="{{asset('backend/button.css')}}" rel="stylesheet" media="all">--}}



</head>

<body>
<div class="page-wrapper">


    <!-- MENU SIDEBAR-->
        @extends('backend.partials.sidebar')
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
            @extends('backend.partials.topbar')
        <!-- HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="">
                <div class="container">


                @yield('content')


                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT-->
        <!-- END PAGE CONTAINER-->


</div>

<!-- Jquery JS-->
<script src="{{asset('backend/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap JS-->
<script src="{{asset('backend/popper.min.js')}}"></script>
<script src="{{asset('backend/bootstrap.min.js')}}"></script>
<!-- Vendor JS       -->
<script src="{{asset('backend/slick.min.js')}}"></script>
<script src="{{asset('backend/wow.min.js')}}"></script>
<script src="{{asset('backend/animsition.min.js')}}"></script>
<script src="{{asset('backend/bootstrap-progressbar.min.js')}}">
</script>
<script src="{{asset('backend/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('backend/jquery.counterup.min.js')}}"></script>
<script src="{{asset('backend/circle-progress.min.js')}}"></script>
<script src="{{asset('backend/perfect-scrollbar.js')}}"></script>
<script src="{{asset('backend/Chart.bundle.min.js')}}"></script>
<script src="{{asset('backend/select2.min.js')}}"></script>


<!-- Main JS-->
<script src="{{asset('backend/main.js')}}"></script>
<script src="{{asset('js/BsMultiSelect.js')}}"></script>
</div>
</body>

</html>
<!-- end document-->
