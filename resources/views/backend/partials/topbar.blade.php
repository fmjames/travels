<header class="header-desktop">
                <nav class="navbar navbar-expand-lg navbar-dark probootstrap_navbar" id="probootstrap-navbar" style="background-color: crimson">
                    <div class="container">
                        <a class="navbar-brand" href="{{route('index')}}">Travels</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
                            <span><i class="ion-navicon"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="probootstrap-menu">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active"><a class="nav-link" href="{{route('admin-index')}}">Admin Panel</a></li>
                                <li class="nav-item active"><a class="nav-link" href="{{route('index')}}">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="city-guides.html">City Guides</a></li>
                                <li class="nav-item"><a class="nav-link" href="services.html">Services</a></li>
                                <li class="nav-item"><a class="nav-link" href="travel.html">Travel With Us</a></li>
                                <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>

                                @auth()
                                    {{--                    <li class="nav-item active"><a class="nav-link" href="{{route('profile')}}">Profile</a></li>--}}
                                    {{--                    <li class="nav-item active"><a class="nav-link" href="{{route('logout')}}">Logout</a></li>--}}

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{auth()->user()->name}}
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{route('profile')}}">Profile</a>
                                            <a class="dropdown-item" href="{{route('purchsae-history')}}">Purchase History</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                                        </div>
                                    </li>

                                @endauth


                                @guest()
                                    <li class="nav-item active"><a class="nav-link" href="{{route('login')}}">Login</a></li>
                                    <li class="nav-item active"><a class="nav-link" href="{{route('register')}}">Register</a></li>
                                @endguest



                            </ul>
                        </div>
                    </div>
                </nav>
</header>
