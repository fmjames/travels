<aside class="menu-sidebar d-none d-lg-block">
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li>
                    <a href="{{route('index')}}">
                        <i class="nav-item far fa-user"></i>User Panel</a>
                </li>
                <li>
                    <a href="{{route('admin-index')}}">
                        <i class="nav-item fas fa-tachometer-alt"></i>Dashboard</a>
                </li>
                <li>
                    <a href="{{route('locations')}}">
                        <i class="nav-item fas fa-location-arrow"></i>Location</a>
                </li>
                <li>
                    <a href="{{route('routes')}}">
                        <i class="nav-item fas fa-road"></i>Route</a>
                </li>
                <li>
                    <a href="{{route('fares')}}">
                        <i class="nav-item fas fa-money"></i>Fare</a>
                </li>
                <li>
                    <a href="{{route('buses')}}">
                        <i class="nav-item fas fa-bus"></i>Bus</a>
                </li>
                <li>
                    <a href="{{route('tracks')}}">
                        <i class="nav-item fas fa-route"></i>Track</a>
                </li>
                <li>
                    <a href="#">
                        <i class="nav-item fas fa-dollar"></i>Transaction</a>
                </li>



{{--                <div class="dropdown">--}}
{{--                    <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown">--}}
{{--                        Add Info--}}
{{--                        <span class="caret"></span>--}}
{{--                    </button>--}}
{{--                    <ul class="dropdown-menu">--}}
{{--                        <li>--}}
{{--                            <a href="{{route('locations')}}" class="fas fa-location-arrow">Locations</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="{{route('routes')}}" class="fas fa-road">Routes</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="{{route('buses')}}" class="fas fa-bus">Buses</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="{{route('tracks')}}" class="fas fa-route">Tracks</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#" class="fas fa-dollar">Transactions</a>--}}
{{--                        </li>--}}

{{--                    </ul>--}}
{{--                </div>--}}




            </ul>
        </nav>
    </div>
</aside>
