<?php

namespace App\Providers;

use App\Events\TracksCreated;
use App\Events\TracksDeleted;
use App\Events\TracksUpdated;
use App\Listeners\TracksCacheListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        TracksCreated::class => [
            TracksCacheListener::class,
        ],
        TracksUpdated::class => [
            TracksCacheListener::class,
        ],
        TracksDeleted::class => [
            TracksCacheListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
