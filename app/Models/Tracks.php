<?php

namespace App\Models;

use App\Events\TracksCreated;
use App\Events\TracksDeleted;
use App\Events\TracksUpdated;
use Illuminate\Database\Eloquent\Model;

class Tracks extends Model
{
    //
    protected $guarded=[];

    protected $dispatchesEvents=[
        'created'=>TracksCreated::class,
        'created'=>TracksUpdated::class,
        'created'=>TracksDeleted::class,
    ];

    public function buses_info(){
        return $this->hasOne(Buses::class,'id','buses_id');
    }

    public function routes_info(){
        return $this->hasOne(Routes::class,'id','routes_id');
    }
}
