<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fares extends Model
{
    //
    protected $guarded=[];

    public function location_from(){
        return $this->hasOne(Locations::class, 'id', 'from');
    }
    public function location_to(){
        return $this->hasOne(Locations::class, 'id', 'to');
    }
}
