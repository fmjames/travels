<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buses extends Model
{
    //
    protected $guarded = [];

    public function false_data(){
        return $this->hasOne(Locations::class, 'id', 'id');
    }
}
