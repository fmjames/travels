<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Routes extends Model
{
    //
    protected $guarded = [
    ];

//    public function start_loc()
//    {
//        return $this->hasMany(Locations::class('id','start_loc_id'));
//    }
//
//    public function end_loc()
//    {
//        return $this->hasMany(Locations::class('id','end_loc_id'));
//    }

    public function location_start(){
    return $this->hasOne(Locations::class, 'id', 'start_loc');
}
public function location_end(){
    return $this->hasOne(Locations::class, 'id', 'end_loc');
}
    public function ending_locations()
    {
        return $this->hasMany(Locations::class,'id','ending_points');
}
//public function buses(){
//    return $this->belongsTo(Buses::class, 'id','routes_id');
//}

}
