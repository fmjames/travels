<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    //
    protected $guarded=[];

    public function tracks_info(){
        return $this->hasOne(Tracks::class,'id','tracks_id');
    }
}
