<?php

namespace App\Listeners;

use App\Models\Tracks;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TracksCacheListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        cache()->forget('tracks_data');
        $tracks_data=Tracks::with('buses_info','routes_info','routes_info.location_start','routes_info.location_end')->get();
        cache()->forever('tracks_data', $tracks_data);
    }
}
