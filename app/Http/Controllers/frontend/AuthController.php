<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Mail\verificationemail;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    //
    public function register(){
        return view('frontend.sections.register');
    }
    public function processregister(Request $request){
        $this->validate($request,[
           'full_name'=>'required',
           'email'=>'required|email|unique:users,email',
           'phone_number'=>'required|min:11|unique:users,phone_number',
           'password'=>'required|min:6',
        ]);
        $regInfo=[
          'name'=>$request->input('full_name'),
          'email'=>strtolower($request->input('email')),
          'phone_number'=>$request->input('phone_number'),
          'password'=>bcrypt($request->input('password')),
          'email_verification_token'=>Str::random(32)
        ];
        try {
            $user=Users::create($regInfo);
            Mail::to($user->email)->queue(new verificationemail($user));
            $this->setSuccessMessage('Account Creates Successfully. Please Check your mail to verify.');
            return redirect()->route('login');
        }catch (\Exception $exception){
            $this->setErrorMessage($exception->getMessage());

            return redirect()->back();
        }

    }
    public function login(){
        return view('frontend.sections.login');
    }

    public function processlogin(Request $request)
    {
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6',
        ]);

        $credentials=$request->except(['_token']);

        if (Auth::attempt($credentials)){
            $user=\auth()->user();
            if ($user->email_verified==0){
                $this->setErrorMessage('Please go to your email & Verify your account first.');
                \auth()->logout();
                return redirect()->route('login');
            }
            return redirect()->route('index');
        }
            $this->setErrorMessage('Invalid Credentials.');
        return redirect()->back();

    }

    public function logout(){
        Auth::logout();

        $this->setSuccessMessage('Logged Out.');
        return redirect()->route('login');
    }

    public function relogin(Request $request){

        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6',
        ]);

        $credentials=$request->except(['_token']);

        if (Auth::attempt($credentials)){
            return redirect()->route('preview');
        }
        $this->setErrorMessage('Incorrect Credentials.');
        return redirect()->back();
    }

    public function verify($token = null)
    {
        if ($token==null){
            $this->setErrorMessage('Invalid Action');
            return redirect()->route('login');
        }
        $user=Users::where('email_verification_token', $token)->first();
        if ($user==null){
            $this->setErrorMessage('Invalid Action');
            return redirect()->route('login');
        }
        $user->update([
            'email_verified'=>1,
            'email_verified_at'=>Carbon::now(),
            'email_verification_token'=>''
        ]);
        return redirect()->route('index');
    }
}
