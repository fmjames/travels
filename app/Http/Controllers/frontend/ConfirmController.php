<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Bookings;
use App\Models\Locations;
use App\Models\Routes;
use App\Models\Tracks;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ConfirmController extends Controller
{
    //
    public function booking(Request $request){

        $tracksid=$request->tracks_info;
        $date=$request->date;
        $seatID = $request->selected_seat;
        $fare = array_sum($request->seat_fare);
        $ticketId = Str::random(10);

        $tracksdata=Tracks::with('buses_info','routes_info','routes_info.location_start','routes_info.location_end')
            ->where('id', $tracksid)->first();
        session()->put('booking',[
            $tracksdata,$date,$fare,$seatID,$ticketId
        ]);
//            dd(session('booking'));
//        return view('frontend.sections.confirm', compact('tracksdata','date','seatID','fare','ticketId'));
        return redirect()->route('preview');
    }

    public function getbooking()
    {
        return view('frontend.sections.preview');
    }

    public function confirm()
    {
        $seats=\session()->get('booking')[3];
        $jsonseat=json_encode($seats);
//        dd($b);

        $confirm_data=[
            'user_id'=>\auth()->id(),
            'tracks_id'=>\session()->get('booking')[0]->id,
            'journey_date'=>\session()->get('booking')[1],
//            'seat_id'=>\session()->get('booking')[3],
            'seat_id'=>$jsonseat,
            'total_fare'=>\session()->get('booking')[2],
            'ticket_id'=>\session()->get('booking')[4]
        ];
//        dd($confirm_data);
        try {
            Bookings::create($confirm_data);
            \session()->forget('booking');
            $this->setSuccessMessage('Ticket Booked Successfully.');
            return redirect()->route('done');
        }catch (\Exception $exception){
            $this->setErrorMessage($exception->getMessage());
            return redirect()->back();
        }
    }

    public function done()
    {
        return view('frontend.sections.confirm');
    }
}
