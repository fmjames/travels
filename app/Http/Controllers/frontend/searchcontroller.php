<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Bookings;
use App\Models\Buses;
use App\Models\Fares;
use App\Models\Locations;
use App\Models\Routes;
use App\Models\Tracks;
use Illuminate\Http\Request;

class searchcontroller extends Controller
{
    //

    public function index(){
        $locations=Locations::all();
        return view('frontend.sections.index', compact('locations'));
    }

    public function routes($id){
        $locations=Locations::all();
        $routes = Routes::with('ending_locations')->whereJsonContains("starting_points",$id)->get();
        $test=[];
        foreach($routes as $single_location){
            foreach(json_decode($single_location->ending_points) as $edp){
                foreach ($locations as $sin_loc){
                    if($edp == $sin_loc->id){
                        $test[]= $sin_loc;

                    }
                }
                array_push($test);
            }
        }

        return response()->json($test);
    }

    public function search(Request $request){
//        dd($request->all());
        $rules=[
            'start_loc'=>'required',
            'date'=>'required'
        ];
        $customMessage=[
          'start_loc.required'=>'Please select a location.',
            'date.required'=>'Please select a date.'
        ];
        $this->validate($request, $rules, $customMessage);

        $start_loc=$request->start_loc;
        $end_loc=$request->end_loc;
        $coach_type=$request->coach_type;
        $date=$request->date;
        $locations=Locations::all();
        $buses=Buses::all();
        $fares=Fares::with('location_from')->where("coach_type",$coach_type)->where("from",$start_loc)->where("to",$end_loc)->get();
//        dd($fares);
        $route_info=Routes::with('location_start')->whereJsonContains("starting_points",$start_loc)->whereJsonContains("ending_points",$end_loc)->get();

        $test_fare='';
        foreach ($fares as $single_fare){
            $test_fare=$single_fare->fare;
        }
//      dd($request->all());

        //Extracting track data with route id & date
        $tracks_check = [];
        foreach ($route_info as $single_route) {
            $tracks_check[] = Tracks::with('buses_info', 'routes_info', 'routes_info.location_start', 'routes_info.location_end')
                ->where('routes_id', $single_route->id)
                ->whereJsonContains('dates', $date)
                ->get();

        }
//        dd($tracks_check);
        //Extracting buses id from track table
        $bus_ids=[];
        foreach ($tracks_check as $single_track){
            foreach ($single_track as $track_info){
                $bus_ids[]=$track_info->buses_id;
            }
        }
//        dd($bus_ids);
        //Extracting filtered buses id with coach type
        $bus_sort_id=[];
        foreach ($bus_ids as $s_bus_id){
            foreach ($buses as $single_bus){
                if ($s_bus_id == $single_bus->id && $coach_type == $single_bus->coach_type){
                    $bus_sort_id[]=$s_bus_id;
                }
            }
        }
        //Extracting track data with filtered route, bus & coach type
        $sorted_track=[];
        foreach ($bus_sort_id as $single_s_bus_id){
            foreach ($route_info as $single_route){
                foreach ($tracks_check as $single_track){
                    foreach ($single_track as $track_info){
                        if ($track_info->buses_id == $single_s_bus_id && $track_info->routes_id == $single_route->id){
                            $sorted_track[]=$track_info;
                    }
                }

                }
            }

        }
//        dd($sorted_track);


//        if ($tracks_check->isEmpty()) {
//            dd($single_track);
            if ($sorted_track===0) {
                $this->setErrorMessage('No bus found.');
                return view('frontend.sections.search', compact('start_loc', 'end_loc', 'locations', 'sorted_track', 'date', 'coach_type'));
            } else {
                return view('frontend.sections.search', compact('start_loc', 'end_loc', 'locations', 'sorted_track', 'date', 'test_fare', 'coach_type'));
            }





    }

        public function show_bus($id, $date){
//        dd($date);
            $tracksdata=Tracks::with('buses_info','routes_info','routes_info.location_start','routes_info.location_end')
                ->where('id', $id)->first();
//            dd($tracksdata);
            $seat_number=($tracksdata->buses_info->total_seat_no)/4;
//            dd($seat_number);
            $col1=1;
            $col2=2;
            $col3=3;
            $col4=4;

            $all_seats=array();
            $booked_seats=Bookings::where('tracks_id',$id)->where('journey_date',$date)->get();
//            dd($booked_seats);
            foreach($booked_seats as $key=>$seat_Id){
                $all_seats=array_merge($all_seats,json_decode($seat_Id->seat_id));
            }
//            dd($all_seats);

            return view('frontend.sections.seat-select', compact('id','date', 'tracksdata','seat_number','all_seats','col1','col2','col3','col4'));
        }

        public function seatFare($id){
//        $hello=$request->id;
            return response()->json($id);
        }


}
