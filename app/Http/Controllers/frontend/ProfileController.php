<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Bookings;
use App\Models\Tracks;
use App\Models\Users;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    public function view()
    {
        return view('frontend.sections.profile');
    }

    public function purchasehistory()
    {
        $userID=auth()->id();
        $ticket_info=Bookings::where('user_id', $userID)->get();
//        foreach($ticket_info as $single_ticket){
//        $tracks_ID=json_encode($single_ticket->tracks_id);}
//        dd($ticket_info);
//        $tracksdata=Tracks::with('buses_info','routes_info','routes_info.location_start','routes_info.location_end')
//            ->where('id', $ticket_info)->first();

//        $trackID= $ticket_info->tracks_id;
//        dd($trackID);
        $tracksdata=Bookings::with('tracks_info','tracks_info.buses_info','tracks_info.routes_info','tracks_info.routes_info.location_start','tracks_info.routes_info.location_end')
            ->get();
//dd($tracksdata);



        return view('frontend.sections.purchasehistory', compact('ticket_info','tracksdata'));
    }

    public function delete($id)
    {
        $ticket_info=Bookings::find($id);
        $ticket_info->delete();
        $this->setSuccessMessage('Ticket Booking Deleted.');
        return redirect()->back();
    }
}
