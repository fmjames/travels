<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Buses;
use App\Models\Routes;
use App\Models\Tracks;
use Illuminate\Http\Request;

class trackscontroller extends Controller
{
    //
    public function info(){
        $buses_data=Buses::all();
        $route_data=Routes::with('location_start','location_end')->get();
//        $tracks_data=Tracks::with('buses_info','routes_info','routes_info.location_start','routes_info.location_end')->get();
        $tracks_data=cache('tracks_data', function (){
           return  Tracks::with('buses_info','routes_info','routes_info.location_start','routes_info.location_end')->get();
        });
        return view('backend.sections.tracks', compact('tracks_data','buses_data', 'route_data'));
    }

    public function insert(Request $request){
//        dd($request->all());

        $validation=$request->validate([
            'buses_id'=>'required',
            'routes_id'=>'required',
            'date'=>'required',
            'dept_time'=>'required',
            'arrv_time'=>'required'
        ]);

        $date = explode(',', $request->date);
        $jsonDate = json_encode($date);
//        dd($jsonDate);

        Tracks::create([
            'buses_id'=>$request->buses_id,
            'routes_id'=>$request->routes_id,
            'dates'=>$jsonDate,
            'departure_time'=>$request->dept_time,
            'arrival_time'=>$request->arrv_time
        ]);
        return redirect()->back()->with('message','Tracks Created Successfully.');
    }

    public function edit($id){
//        dd($id);
        $tracks_data=Tracks::find($id);
        $buses_data=Buses::all();
        $routes_data=Routes::all();
//        dd($tracks_data);
        return view('backend.sections.tracks_edit', compact('tracks_data','routes_data','buses_data'));
    }

    public function update(Request $request, $id){

        $date = explode(',', $request->date);
        $jsonDate = json_encode($date);

        $tracks_info=[
            'buses_id'=>$request->input('buses_id'),
            'routes_id'=>$request->input('routes_id'),
            'dates'=>$jsonDate,
            'departure_time'=>$request->input('dept_time'),
            'arrival_time'=>$request->input('arrv_time')
        ];
        $track_update=Tracks::where('id', $id)->update($tracks_info);
        return redirect()->route('tracks');
    }

    public function delete($id){
        $track_data=Tracks::find($id);
        $track_data->delete();
        return redirect()->route('tracks');
    }
}
