<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Locations;
use Illuminate\Http\Request;

class locationscontroller extends Controller
{
    //
    public function info(){
        return view('backend.sections.locations')->with('loc_data', Locations::all());
    }

    public function insert(Request $request){

        $validation=$request->validate([
            'location_name'=>'required'
        ]);
//        dd($request->all());
        Locations::create([
            'loc'=>$request->location_name,
        ]);
        return redirect()->route('locations')->with('message','Location Created Successfully.');
    }

    public function edit($loc_id){

//        dd($loc_id);
        return view('backend.sections.locationedit')->with('loc_id',Locations::find($loc_id));
    }

//    public function update($loc_id){
//
//        $data=request()->all();
//        $loc_up=Locations::find($loc_id);
//        $loc_up->start=$data['start_location'];
//        $loc_up->end=$data['end_location'];
//        $loc_up->save();
//        return redirect('/add-info/locations');
//    }


    public function update(Request $request,$id)
    {

        $validation=$request->validate([
            'location_name'=>'required'
        ]);

        $locationData=[
            'loc'=> $request->input('location_name'),

        ];
        $locdata=Locations::where('id',$id)->update($locationData);
        return redirect()->route('locations')->with('message','Location Edited Successfully.');
    }


    public function delete($loc_id){
        $loc_up=Locations::find($loc_id);
        $loc_up->delete();
        return redirect('/add-info/locations');
    }
}
