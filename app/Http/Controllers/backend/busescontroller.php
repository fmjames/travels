<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Buses;
use App\Models\Locations;
use App\Models\Routes;
use Illuminate\Http\Request;

class busescontroller extends Controller
{
    //
    public function info(){
//        $buses_data= Buses::with('destination_data', 'destination_data.location_start', 'destination_data.location_end')->get();
//      $routes_data= Routes::with('location_start', 'location_end')->get();
//        $loc_data=Locations::all();
//        dd($routes_data);
//        return view('backend.sections.buses', compact('buses_data','routes_data'));
        $buses_data=Buses::all();
        return view('backend.sections.buses', compact('buses_data'));
    }

    public function insert(Request $request){
//dd($request->all());
        $validation=$request->validate([
            'coach_id'=>'required|max:4',
            'bus_type'=>'required',
            'b_seat_no'=>'required'
        ]);

        Buses::create([
            'coach_id'=>$request->coach_id,
            'coach_type'=>$request->bus_type,
            'total_seat_no'=>$request->b_seat_no
        ]);
        return redirect()->route('buses')->with('message','Bus Created Successfully.');
    }

    public function edit($buses_id){
//        dd($buses_id);
        $buses_info=Buses::find($buses_id);
        return view('backend.sections.buses_edit',compact('buses_info'));
    }

    public function update(Request $request, $id){

        $validation=$request->validate([
            'coach_id'=>'required|max:4',
            'bus_type'=>'required',
            'b_seat_no'=>'required'
        ]);

        $buses_info=[
            'coach_id'=>$request->input('coach_id'),
            'coach_type'=>$request->input('bus_type'),
            'total_seat_no'=>$request->input('b_seat_no')
        ];
        $bus_update=Buses::where('id', $id)->update($buses_info);
        return redirect()->route('buses')->with('message','Bus Edited Successfully.');
    }

    public function delete($id){
        $buses_info=Buses::find($id);
        $buses_info->delete();
        return redirect()->back();
    }
}
