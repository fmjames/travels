<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Locations;
use App\Models\Routes;
use Illuminate\Http\Request;

class routescontroller extends Controller
{
    //
    public function info()
    {
        $route_data=Routes::with('location_start', 'location_end')->get();
        $loc_data=Locations::all();
//      dd($loc_data);
        return view('backend.sections.routes',compact('route_data','loc_data'));
    }

    public function insert(Request $request)
    {
//        dd($request->all());
        Routes::create([
            'start_loc' => $request->start_location,
            'end_loc' => $request->end_location,
            'starting_points' => json_encode($request->start_points),
            'ending_points' => json_encode($request->end_points),
        ]);
        return redirect()->back();
    }

    public function edit($routes_id){
        $routes_data=Routes::find($routes_id);
        $loc_data=Locations::all();
//        dd($routes_data->all());
        return view('backend.sections.routes_edit', compact('routes_data', 'loc_data'));

    }

    public function update(Request $request, $id){
        $routes_data=[
          'start_loc'=>$request->input('start_location')  ,
            'end_loc'=>$request->input('end_location'),
            'starting_points' => json_encode($request->start_points),
            'ending_points' => json_encode($request->end_points),
        ];
        $routeupdate=Routes::where('id',$id)->update($routes_data);
        return redirect()->route('routes');
    }

    public function delete($id){
        $route_data=Routes::find($id);
        $route_data->delete();
        return redirect()->route('routes');
    }
}
