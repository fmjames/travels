<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Fares;
use App\Models\Locations;
use Illuminate\Http\Request;

class farecontroller extends Controller
{
    //
    public function info()
    {
        $location=Locations::all();
        $fares=Fares::with('location_from', 'location_to')->get();
//        dd($location);
        return view('backend.sections.fares',compact('location','fares'))->with('start_location');
    }

    public function insert(Request $request)
    {
//        dd($request->all());
        $validation=$request->validate([
            'start_location'=>'required',
            'end_location'=>'required',
            'coach_type'=>'required',
            'fare'=>'required',
        ]);
        Fares::create([
            'from'=>$request->start_location,
            'to'=>$request->end_location,
            'coach_type'=>$request->coach_type,
            'fare'=>$request->fare,
        ]);
        return redirect()->back();
    }

    public function edit($fare_id)
    {
        $locations=Locations::all();
        return view('backend.sections.fare_edit', compact('locations'))->with('fare_id',Fares::find($fare_id));
    }

    public function update(Request $request,$fare_id)
    {
        $validation=$request->validate([
            'start_location'=>'required',
            'end_location'=>'required',
            'coach_type'=>'required',
            'fare'=>'required',
        ]);
        $fare_details=[
            'from'=>$request->start_location,
            'to'=>$request->end_location,
            'coach_type'=>$request->coach_type,
            'fare'=>$request->fare,

        ];
        $update=Fares::where('id',$fare_id)->update($fare_details);
        return redirect()->route('fares');

    }

    public function delete($fare_id)
    {
        $delete=Fares::where('id',$fare_id)->delete();
        return redirect()->back();
    }
}
