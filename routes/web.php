<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','frontend\searchcontroller@index')->name('index');


//<<--Ticket Checking-->>
Route::get('/search/{id}','frontend\searchcontroller@routes')->name('search-routes');
Route::post('/show-tracks','frontend\searchcontroller@search')->name('show-tracks');
Route::get('/show-bus/{id}/{date}','frontend\searchcontroller@show_bus')->name('show-bus');
Route::get('/seat-fare/{seat_id}','frontend\searchcontroller@seatFare')->name('seat-fare');

//<<--Ticket Confirm-->>
Route::post('/seat-select','frontend\ConfirmController@booking')->name('booking');
Route::get('/seat-select','frontend\ConfirmController@getbooking')->name('preview');
Route::get('/confirm','frontend\ConfirmController@confirm')->name('confirm');
Route::get('/done','frontend\ConfirmController@done')->name('done');



Route::get('/register','frontend\AuthController@register')->name('register');
Route::post('/register','frontend\AuthController@processregister');


Route::get('/login','frontend\AuthController@login')->name('login');
Route::post('/login','frontend\AuthController@processlogin');
Route::get('/logout','frontend\AuthController@logout')->name('logout');

Route::get('/verify/{token}','frontend\AuthController@verify')->name('verify');

Route::get('/profile','frontend\ProfileController@view')->name('profile');
Route::get('/purchase-history','frontend\ProfileController@purchasehistory')->name('purchsae-history');
Route::get('/delete-ticket/{id}','frontend\ProfileController@delete')->name('delete-ticket');

//Login check while user wants to booked a seat
Route::post('/relogin','frontend\AuthController@relogin')->name('relogin');



//<<--Backend Controllers-->>

Route::get('/admin','backend\admincontroller@index')->name('admin-index');

//<<--Location Controllers-->>
Route::get('/add-info/locations','backend\locationscontroller@info')->name('locations');
Route::post('/add-info/add-location','backend\locationscontroller@insert')->name('loc-insert');
Route::get('/add-info/location-edit/{loc_id}','backend\locationscontroller@edit')->name('loc-edit');
Route::post('/add-info/location-update/{loc_id}','backend\locationscontroller@update')->name('loc-update');
Route::get('/add-info/location-delete/{loc_info}','backend\locationscontroller@delete')->name('loc-delete');
//<<--Routes Controllers-->>
Route::get('/add-info/routes','backend\routescontroller@info')->name('routes');
Route::post('/add-info/add-routes','backend\routescontroller@insert')->name('routes_add');
Route::get('/add-info/routes-edit/{route_id}','backend\routescontroller@edit')->name('routes-edit');
Route::post('/add-info/routes-update/{route_id}','backend\routescontroller@update')->name('routes-update');
Route::get('/add-info/routes-delete/{route_id}','backend\routescontroller@delete')->name('routes-delete');
//<<--Buses Controllers-->>
Route::get('/add-info/buses','backend\busescontroller@info')->name('buses');
Route::post('/add-info/add-buses','backend\busescontroller@insert')->name('buses-insert');
Route::get('/add-info/edit-buses/{buses_id}','backend\busescontroller@edit')->name('buses-edit');
Route::post('/add-info/update-buses/{buses_id}','backend\busescontroller@update')->name('buses-update');
Route::get('/add-info/delete-buses/{buses_id}','backend\busescontroller@delete')->name('buses-delete');
//<<--Tracks Controllers-->>
Route::get('/add-info/tracks','backend\trackscontroller@info')->name('tracks');
Route::post('/add-info/add-tracks','backend\trackscontroller@insert')->name('tracks-add');
Route::get('/add-info/edit-tracks/{tracks_id}','backend\trackscontroller@edit')->name('tracks_edit');
Route::post('/add-info/update-tracks/{tracks_id}','backend\trackscontroller@update')->name('tracks_update');
Route::get('/add-info/delete-tracks/{tracks_id}','backend\trackscontroller@delete')->name('tracks_delete');
//<<--Fare Controllers-->>
Route::get('/add-info/fares','backend\farecontroller@info')->name('fares');
Route::post('/add-info/add-fare','backend\farecontroller@insert')->name('fare-insert');
Route::get('/add-info/edit-fare/{fare_id}','backend\farecontroller@edit')->name('fare-edit');
Route::post('/add-info/update-fare/{fare_id}','backend\farecontroller@update')->name('fare-update');
Route::get('/add-info/delete-fare/{fare_id}','backend\farecontroller@delete')->name('fare-delete');


Route::get('/test','backend\TestController@test')->name('test');
//<<--Backend Controllers-->>
