<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Fakeuser::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->sentence(2),
        'email'=>$faker->unique()->safeEmail,
        'password'=>$faker->sentence(1)
    ];
});
