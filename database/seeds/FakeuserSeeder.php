<?php

use Illuminate\Database\Seeder;
use app\Models\fakeuser;

class FakeuserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Models\Fakeuser::class, 10)->create();
    }
}
